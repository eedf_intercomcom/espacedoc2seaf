#!/usr/bin/env python3
# -*- coding: utf-8 -*-
###############################################################################
#       espace-doc2seaf.py
#       
#       Copyright © 2015-2020, Florence Birée <florence@biree.name>
#       
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Télécharger les documents de l'espace documentaire des EEDF pour les mettre
dans un seafile

Mis à jour en 2019 pour la nouvelle version de l'espace doc des EEDF

Ajout en 2020 du support des padlets
"""

__author__ = "Florence Birée"
__version__ = "3.0"
__license__ = "GPLv3"
__copyright__ = "Copyright © 2015-2020, Florence Birée <florence@biree.name>"

import os
import sys
import argparse
import urllib.request
from urllib.parse import urljoin, urlencode, urlparse, urlunparse, quote
import http.cookiejar
import shutil
import lxml.html
import json
import re
import html2text
import io

class PathPreProcessor(object):
    """Effectue des conversions de nom de chemin entre l'arborscence locale et
        celle distante"""
    
    def __init__(self, pathdict=None, pathtable_file=None):
        if pathtable_file is not None:
            pathdict = self.loadfile(pathtable_file)
        if pathdict is None:
            self.table = []
        else:
            self.table = pathdict
    
    def loadfile(self, filename):
        with open(filename, 'rb') as ft:
            ctn = ft.read().decode('utf-8')
        pathtbl = []
        for line in ctn.split('\n'):
            line = line.strip()
            if not line.startswith('#') and '=' in line:
                diststr, locstr = line.split('=')
                diststr = diststr.strip()
                locstr = locstr.strip()
                pathtbl.append((diststr, locstr))
        return pathtbl
    
    def makepath(self, root, dist_path):
        """Return a local path, making convertions"""
        new_path = dist_path
        for (orig, dest) in self.table:
            if orig in new_path:
                new_path = new_path.replace(orig, dest)
        return os.path.join(root, new_path)

class SyncWorker(object):
    """Récupère deupis un objet source des fichiers ou répertoires, 
    
        les synchronise avec les fichiers locaux
    """
    UA = "Mozilla/5.0 (X11; Linux x86_64; rv:80.0) Gecko/20100101 Firefox/80.0"
    
    def __init__(self, root, private_root=None, dry_run=False, adh=None, verbose=False):
        """Créé un nouveau worker
        
            root est le chemin du répertoire des fichiers locaux
            private_root est le chemin du répertoire des fichiers avec authentification
            dry_run affiche le travail à faire, sans le faire.
            adh est un numéro d'adhérent pour accéder aux parties avec authentification
        """
        self.root = root
        self.private_root = private_root
        self.dry_run = dry_run
        self.adh = adh
        self.verbose = verbose
        self._cj = None
        self._opener = None
    
    def do(self, src_list, ppp=None):
        """Synchronise à l'aide des sources de src_list"""
        if ppp is None:
            ppp = PathPreProcessor()
        # initialize a new opener (clear cookies)
        self._cj = http.cookiejar.CookieJar()
        self._opener = urllib.request.build_opener(
            urllib.request.HTTPCookieProcessor(self._cj)
        )
        self._opener.addheaders = [("User-agent", self.UA)]
        
        # sources non-authentifiées
        for src in (s for s in src_list if not s.needauth):
            if self.verbose: print("Source :", src)
            self._do1(src, ppp)
        # sources authentifiées
        if self.private_root is None or self.adh is None:
            return
        for src in (s for s in src_list if s.needauth):
            if self.verbose: print("Source :", src)
            self._do1(src, ppp, auth=True)
        
    
    def _do1(self, src, ppp, auth=False):
        """Récupère les fichiers d'une source"""
        root = self.private_root if auth else self.root 
        
        for node in src.items(self._opener, auth, self.adh):
            path = ppp.makepath(root, node['path'])
            filename = node['filename']
            filepath = os.path.join(path, filename)
            notauthp = os.path.join(self.root, node['path'], node['filename'])
            
            # vérifier l'existence du fichier
            if os.path.exists(filepath):
                # file exists, stop if stoponfirstfile
                if self.verbose: print("Existe déjà : %s" % filepath)
                if src.stoponfirstfile:
                    if self.verbose: print("Donc arrêt")
                    break
            elif auth and os.path.exists(notauthp):
                # file exists in not auth directory, do nothing
                if self.verbose: print("Existe déjà en public : %s" % filepath)
                pass
            else:
                # create the directory if needed
                if not self.dry_run:
                    try:
                        os.makedirs(path)
                    except FileExistsError:
                        pass # not a problem
                # dl the file
                if self.dry_run:
                    print("à télécharger %s" % filepath)
                else:
                    try:
                        with open(filepath, 'wb') as out_file:
                            shutil.copyfileobj(node['fileo'], out_file)
                            print("télécharge %s" % filepath)
                    except OSError:
                        print("Erreur téléchargement %s" % filepath)
                
class EspaceDocSrc(object):
    """Outil de téléchargement des documents de l'espace documentaire"""
    
    url = 'https://www.eedf.fr/espace-documentaire/'
    stoponfirstfile = False
    
    def __init__(self, auth_files=False):
        """Définir auth_files à True pour ne télécharger que les documents authentifiés"""
        self.needauth = auth_files

    def items(self, opener, auth, adh):
        """Générateur renvoyant des nodes {'path', 'filename', 'fileo'}
            pour chaque document de l'espace documentaire
        """
        with opener.open(self.url) as response:
            page = lxml.html.fromstring(response.read())

        dossiers_racines = \
            page.cssselect('#side-dossiers-documents ul')[0].getchildren()
        
        # Lance le téléchargement d'un dossier pour chaque dossier racine
        dossier_url = []
        for li in dossiers_racines:
            a = li.cssselect('a')[0]
            url = a.get('href')
            dossier = DossierSrc(url)
            dossier_url.append(url)
            yield from dossier.items(opener, auth, adh)
        
        # Lance le téléchargement d'un dossier pour chaque dossier campagne
        campagnes = page.cssselect('ul.dossiers')[0].getchildren()
        for li in campagnes:
            a = li.cssselect('a')[0]
            campagne_url = a.get('href')
            if campagne_url.startswith('https://www.eedf.fr/documents/'):
                # ne pas re-télécharger en campagne un dossier déjà existant.
                if not campagne_url in dossier_url:
                    campagne = CampagneSrc(campagne_url)
                    yield from campagne.items(opener, auth, adh)

class DossierSrc:
    """Source pour un dossier (espace doc ou campagne), pouvant contenir des
    sous dossiers
    """

    authurl = 'https://www.eedf.fr/wp-admin/admin-ajax.php'
    prefix = ''

    def __init__(self, url, auth_files=False):
        self.needauth = auth_files
        self.url = url
    
    def items(self, opener, auth, adh):
        """Renvoie les items à télécharger"""
        with opener.open(self.url) as response:
            page = lxml.html.fromstring(response.read())
        main = page.cssselect('main')[0]
        
        # titre du dossier racine
        dossier_racine = self.prefix +  main.cssselect('h2')[0].text
        
        # parcourir les fichiers
        sous_dossiers_list = main.cssselect('.liste-sous-dossiers > li')
        if len(sous_dossiers_list) > 0 :
            for sous_dossier in sous_dossiers_list:
                sous_dossier_nom = sous_dossier.cssselect('h3')[0].text
                
                sous_sous_dossiers_list = sous_dossier.cssselect('.liste-sous-sous-dossiers > li')
                if len(sous_sous_dossiers_list) > 0:
                    for sous_sous_dossier in sous_sous_dossiers_list:
                        sous_sous_dossier_nom = sous_sous_dossier.cssselect('h4')[0].text
                        yield from self._get_doc_list(opener, auth, adh, sous_sous_dossier, 
                            '/'.join((dossier_racine, sous_dossier_nom, 
                                     sous_sous_dossier_nom)))
                else:
                    yield from self._get_doc_list(opener, auth, adh, sous_dossier, 
                        '/'.join((dossier_racine, sous_dossier_nom)))
        else:
            yield from self._get_doc_list(opener, auth, adh, main, dossier_racine)
                        
    def _get_doc_list(self, opener, auth, adh, docroot, path):
        """Renvoie les items depuis une document-list.
        
            docroot: élément contenant la document-list
            path: chemin des dossiers contenant la docroot
        """
        
        doc_list = docroot.cssselect('.document-list li')
        for doc in doc_list:
            nom = doc.cssselect('h4')[0].text
            a = doc.cssselect('.links a.telecharger')[0]
            url = None
            if 'protected' in a.classes and auth:
                # récupérer l'url d'un fichier demandant authentification
                values = {
                    "action": "eedf_media_category_protected_submit",
                    "media_id": a.get('data-media-id'),
                    "password": adh,
                }
                data = urlencode(values)
                data = data.encode('utf-8') # data should be bytes
                req = urllib.request.Request(self.authurl, data)
                with opener.open(req) as response:
                    resp_data = json.loads(response.read().decode('utf-8'))
                
                if resp_data['success']:
                    url = resp_data['url_media']
                
            elif not 'protected' in a.classes and not auth:
                url = a.get('href')
            
            if url:
                # get extension
                ext = os.path.splitext(url)[1]
                #print(path + '/' + nom + ext + '\t' + url)
                
                try:
                    urlp = list(urlparse(url))
                    urlp[2] = quote(urlp[2]) # encode the path part
                    url = urlunparse(urlp)
                    with opener.open(url) as response:
                        # récupérer le nom de fichier dans les en-têtes de la
                        # réponse
                        filename = nom + ext
                        try:
                            h_list = response.info()['Content-Disposition'].split(';')
                        except AttributeError:
                            h_list = []
                        for h_data in h_list:
                            if h_data.strip().startswith('filename='):
                                filename = h_data.strip().split('=')[1].\
                                                        replace('"', '')
                        yield {
                            'path': path, 
                            'filename': filename.replace('/', '-'), 
                            'fileo': response,
                        }
                except UnicodeEncodeError:
                    print('Erreur d\'encodage, téléchargez manuellement '
                            '%s dans %s' % (url, path))
                except urllib.error.HTTPError:
                    print('Erreur lors de l\'accès à ' + url)
                    raise

class CampagneSrc(DossierSrc):
    """Création d'une classe campagne, pour donner un titre commençant par 0-
    """
    prefix = '0-'    

class PadletSrc:
    """Classe padlet, permettant de récupérer le contenu d'un padlet"""
    BASE = '0-Padlets'
    WALL_ID_RE = re.compile(r'"wall":\{"id":(\d+),')
    API_BASE = "https://padlet.com/api/3/"
    API_SECTIONS = API_BASE + 'wall_sections?wall_id={wall_id}'
    API_WISHES = API_BASE + 'wishes?wall_id={wall_id}'
    STORAGE = 'https://padlet-uploads.storage.googleapis.com/'
    PADLET = 'padlet.com/pedagogieeedf/'
    WALLID = {
        'https://padlet.com/pedagogieeedf/responsable_unite': '71906696',
        'https://padlet.com/pedagogieeedf/responsable_regional': '71906683',
        'https://padlet.com/pedagogieeedf/Lutin': '72482523',
        'https://padlet.com/pedagogieeedf/CentresEtTerrains': '93734151',
        'https://padlet.com/pedagogieeedf/Louveteau': '72482436',
        'https://padlet.com/pedagogieeedf/eclaireur': '72390304',
        'https://padlet.com/pedagogieeedf/aine': '71692221',
        'https://padlet.com/pedagogieeedf/JAE': '72491506',
        'https://padlet.com/pedagogieeedf/responsable_de_groupe': '71906365',
        'https://padlet.com/pedagogieeedf/covid19': '81559999',
        'https://fr.padlet.com/pedagogieeedf/covid19': '81559999',
        'https://padlet.com/pedagogieeedf/q4lo48tu8dfgfslm': '72390304',
        'https://padlet.com/pedagogieeedf/aseep3gc23m15erz': '71692221',
        'https://padlet.com/pedagogieeedf/q4lo48tu8dfgfslm': '72390304',
        'https://padlet.com/pedagogieeedf/rd9cpst6j692maor': '71906696',
        'https://padlet.com/pedagogieeedf/directeuricedestage': '101236590',
        'https://padlet.com/pedagogieeedf/chartecovid2021': '101309593',
        'https://padlet.com/pedagogieeedf/ete2021': '101102781',
        'https://padlet.com/alexiscorbara/5sfwfoujag7tpswy': '102198520',
    }

    
    the_big_padlet_list = []
    
    stoponfirstfile = False
    
    def __init__(self, url, base=None):
        self.url = url
        self.the_big_padlet_list.append(url)
        self.base = base if base else self.BASE
        self.needauth = False
        self._wall_id = None
        self.name = None
        

    def _get_wall_infos(self, opener):
        """Try to find the wall id and padlet name from the URL"""
        #"wall":{"id":71906696
        with opener.open(self.url) as response:
            page = response.read()
        
        # get id THIS DOESN'T WORK ANYMORE
        #wall_id_match = self.WALL_ID_RE.search(page.decode('utf-8'))
        #if wall_id_match:
        #    self._wall_id = wall_id_match.group(1)
        # dirty workaround, we use a static list of url <-> id
        try:
            self._wall_id = self.WALLID[self.url]
        except KeyError:
            print("Ajouter dans WALLID l'id de " + self.url)
        
        # get name
        page = lxml.html.fromstring(page)
        self.name = page.cssselect('head title')[0].text
        
        # get url (handle redirects)
        if response.geturl() != self.url:
            self.url = response.geturl()
            self.the_big_padlet_list.append(self.url)

    def items(self, opener, auth=False, adh=None):
        """Générateur renvoyant des nodes {'path', 'filename', 'fileo'}
            pour chaque document du padlet
        """
        if not self._wall_id:
            self._get_wall_infos(opener)
            
        # get sections list
        sections = []
        with opener.open(self.API_SECTIONS.format(wall_id=self._wall_id)) as response:
            sections_data = json.loads(response.read().decode('utf-8'))
        for js_section in sections_data['data']:
            sections.append({
                'id': js_section['id'],
                'title': js_section['attributes']['title'],
            })
        
        # get wishes
        with opener.open(self.API_WISHES.format(wall_id=self._wall_id)) as response:
            wishes_data = json.loads(response.read().decode('utf-8'))
        
        for js_wish in wishes_data['data']:
            att = js_wish['attributes']
            # get wish data
            headline = att['headline']
            subject = att['subject']
            body = att['body']
            url = att['attachment']
            section_id = att['wall_section_id']
            
            section_name = None
            # trouver la section
            for section in sections:
                if int(section['id']) == section_id:
                    section_name = section['title']
            if not section_name:
                continue
            
            path = self.base + '/' + self.name + '/' + section_name
            # analyse the wish
            if url and url.startswith(self.STORAGE):
                # we have a file !   
                
                # get filename / extension
                url_parts = url.split('/')
                filename = url_parts[-1]
                ext = os.path.splitext(filename)[1]
                
                if headline and headline != 'Empty':
                    filename = headline + ext
                
                try:
                    #urlp = list(urlparse(url))
                    #urlp[2] = quote(urlp[2]) # encode the path part
                    #url = urlunparse(urlp)
                    with opener.open(url) as response:
                        # récupérer le nom de fichier dans les en-têtes de la
                        # réponse
                        try:
                            h_list = response.info()['Content-Disposition'].split(';')
                        except AttributeError:
                            h_list = []
                        for h_data in h_list:
                            if h_data.strip().startswith('filename='):
                                    filename = h_data.strip().split('=')[1].\
                                                          replace('"', '')
                        yield {
                            'path': path, 
                            'filename': filename.replace('/', '-'), 
                            'fileo': response,
                        }
                except UnicodeEncodeError:
                    print('Erreur d\'encodage, téléchargez manuellement '
                            '%s dans %s' % (url, path))
                except urllib.error.HTTPError:
                    print('Erreur lors de l\'accès à ' + url)
                    raise
            
            elif url and self.PADLET in url:
                # we have another padlet!
                
                # save the big_padlet_list, as we may change it
                old_padlet_list = list(self.the_big_padlet_list)
                
                new_padlet = PadletSrc(url, path)
                new_padlet._get_wall_infos(opener) # check redirect
                
                # check if it's not already somewhere
                if not url in old_padlet_list and not new_padlet.url in old_padlet_list:
                    yield from new_padlet.items(opener)
                
            elif headline and not headline == 'Empty':
                # we have various content… -> md file!
                # name the file
                filename = headline + '.md'
                if body or url:
                    # convert the content from html to markdown
                    content = html2text.html2text(body)
                    if url:
                        if content:
                            content += '\n'
                        content += url
                    fileo = io.BytesIO(content.encode('utf-8'))
                    
                    yield {
                        'path': path,
                        'filename': filename.replace('/', '-'),
                        'fileo': fileo,
                    }
                
def _get_padlets(filename):
    """Return a list of padlets"""
    pllist = []
    with open(filename, 'r') as plfile:
        for line in plfile:
            if line.startswith('https'):
                pllist.append(PadletSrc(line.strip()))
    return pllist

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-d", "--dryrun", 
                help="cherche les nouveautées sans rien télécharger",
                action="store_true")
    parser.add_argument("-v", "--verbose",
                help="affichage plus verbeux",
                action="store_true")
    parser.add_argument("racine", 
                help="le répertoire où télécharger l'espace documentaire")
    parser.add_argument("-n", "--adh",
                help="numéro d'adhérent pour les zones authentifiées")
    parser.add_argument("-p", "--private",
                help="répertoire où télécharger les documents privés")
    parser.add_argument("-t", "--table",
                help="fichier contenant la table de conversion des chemins")
    parser.add_argument("--padlets",
                help="fichier contenant la liste des padlets")
    args = parser.parse_args()

    if not os.path.exists(args.racine):
        print("Erreur : %s doit exister" % args.racine)
        sys.exit(2)
    else:
        
        if args.table:
            ppp = PathPreProcessor(pathtable_file=args.table)
        else:
            ppp = None
        
        src = [
            EspaceDocSrc(),
            EspaceDocSrc(auth_files=True),
        ]
        
        if args.padlets:
            src += _get_padlets(args.padlets)
        
        w = SyncWorker(
            args.racine,
            private_root = args.private,
            dry_run = args.dryrun,
            adh = args.adh,
            verbose = args.verbose,
        )
        w.do(src, ppp=ppp)


