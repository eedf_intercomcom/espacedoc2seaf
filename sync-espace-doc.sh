#!/bin/sh
# script de synchronisation de l'espace doc avec Seafile

echo "Synchronisation espace doc du `date`"

# vérification que seafile est bien lancé
if seaf-cli status 2>/dev/null
then
	echo "Client seafile ok"
else
	echo "Lancement du client seafile"
	seaf-cli start
fi

# lancement de la synchronisation
/home/espacedoc/bin/ed2s.sh

# ajout d'une ligne vide pour faire bien dans les logs
echo

