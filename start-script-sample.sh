#!/bin/sh
# Script pour lancer espace-doc2seaf avec les bons paramètres
NUM_ADH=YOURNUM
EDPATH=/ton/dossier/espace/doc/
TDC=./table-des-chemins
PADLETS=./padlets.txt

if [ u$1 = u'-d' ]
then
    dryrun='-d'
else
    dryrun=''
fi
                
./espacedoc2seaf.py -n $NUM_ADH \
    -p $EDPATH $EDPATH \
    -t $TDC --padlets $PADLETS \
    $dryrun

